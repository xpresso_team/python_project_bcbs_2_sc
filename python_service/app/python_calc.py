# Calculator class


class Calculator:
    """
    Calculator class
    """

    def __init__(self, a, b):
        """
        Initialization
        :param a: float
        :param b: float
        """
        self.a = a
        self.b = b

    def add(self):
        return self.a + self.b
